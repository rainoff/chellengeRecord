import * as React from "react";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Copyright from "@/src/components/Copyright";

export async function getStaticPaths() {
  return {
    paths: [
      { params: { project: "1" } },
      { params: { project: "2" } },
      { params: { project: "3" } },
    ],
    fallback: false, // can also be true or 'blocking'
  };
}

// `getStaticPaths` requires using `getStaticProps`
export async function getStaticProps({ params }) {
  return {
    // Passed to the page component as props
    props: { params },
  };
}

export default function Project({ params }) {
  return (
    <>
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={3}>
          Name {params.project}
        </Grid>
        <Copyright sx={{ pt: 4 }} />
      </Container>
    </>
  );
}
