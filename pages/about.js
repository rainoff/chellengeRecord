import * as React from "react";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Copyright from "@/src/components/Copyright";

export default function About() {
  return (
    <>
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={3}>
          123
        </Grid>
        <Copyright sx={{ pt: 4 }} />
      </Container>
    </>
  );
}
