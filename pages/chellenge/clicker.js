import { useState, useEffect } from "react";
import Button from "@mui/material/Button";

const clicker = () => {
  const [count, setCount] = useState(0);
  const [color, setColor] = useState("black");
  const handleColorChange = (props) => {
    if (props === "plus" && count < 30) {
      setCount(count + 1);
    } else if (props === "minus" && count > 0) {
      setCount(count - 1);
    } else {
      return;
    }
    if (count > 15 && color !== "red") {
      setColor("red");
    } else if (count <= 15 && color !== "blue") {
      setColor("blue");
    }
  };
  return (
    <div className={color}>
      {count}
      <Button
        variant="text"
        color="primary"
        onClick={() => handleColorChange("plus")}
      >
        Click plus
      </Button>
      <Button
        variant="text"
        color="primary"
        onClick={() => handleColorChange("minus")}
      >
        Click Minus
      </Button>
    </div>
  );
};

export default clicker;
