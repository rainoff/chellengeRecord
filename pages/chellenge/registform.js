import { useState } from "react";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormHelperText from "@mui/material/FormHelperText";
import { Input, Button, FormGroup } from "@mui/material";

const registForm = () => {
  const [submitted, setsubmitted] = useState(false);
  const [valid, setValid] = useState(false);
  const [values, setValues] = useState({
    firstName: "",
    lastName: "",
    email: "",
  });
  const handleFirstNameEnter = (event) => {
    setValues({ ...values, firstName: event.target.value });
  };
  const handleLastNameEnter = (event) => {
    setValues({ ...values, lastName: event.target.value });
  };
  const handleEmailEnter = (event) => {
    setValues({ ...values, email: event.target.value });
  };
  const handleSubmit = (event) => {
    console.log("submit");
    event.preventDefault();
    if (values.firstName && values.lastName && values.email) {
      setValid(true);
    }
    setsubmitted(true);
  };

  return (
    <>
      <form id="form" onSubmit={handleSubmit}>
        <FormControl>
          {submitted && valid ? (
            <div className="success-message">success</div>
          ) : null}
          <FormLabel>First Name</FormLabel>
          <Input
            disabled={submitted && valid}
            id="firstName"
            name="firstName"
            value={values.firstName}
            onChange={handleFirstNameEnter}
          />
          {submitted && !values.firstName ? (
            <div>plz enter there col</div>
          ) : null}
        </FormControl>
        <FormControl>
          <FormLabel>Last Name</FormLabel>
          <Input
            disabled={submitted && valid}
            id="lastName"
            name="lastName"
            value={values.lastName}
            onChange={handleLastNameEnter}
          />
          {submitted && !values.lastName ? (
            <div>plz enter there col</div>
          ) : null}
        </FormControl>
        <FormControl>
          <FormLabel>Email</FormLabel>
          <Input
            disabled={submitted && valid}
            id="email"
            name="email"
            value={values.email}
            onChange={handleEmailEnter}
          />
          {submitted && !values.email ? <div>plz enter there col</div> : null}
        </FormControl>
        <Button aria-label="fingerprint" color="secondary" type="submit">
          Register
        </Button>
      </form>
    </>
  );
};

export default registForm;
