import { useEffect, useState } from "react";
import MOCK_DATA from "../../src/data/MOCK_DATA.json";

const searchFilter = () => {
  const [search, setSearch] = useState("");

  return (
    <>
      <div>type content = {search}</div>
      <input
        name="search"
        type="text"
        placeholder="search..."
        onChange={(event) => {
          setSearch(event.target.value);
        }}
      />
      {MOCK_DATA.filter((val) => {
        if (search === "") {
          return val;
        } else if (
          val.first_name.toLowerCase().includes(search.toLowerCase())
        ) {
          return val;
        }
      }).map((val) => {
        return (
          <div key={val.id} className="userMan">
            <div>{val.first_name}</div>
          </div>
        );
      })}
    </>
  );
};

export default searchFilter;
