import { dividerClasses } from "@mui/material";
import { useState } from "react";

const questions = [
  {
    questionText: "What is the capital of France?",
    answerOptions: [
      { answerText: "New York", isCorrect: false },
      { answerText: "London", isCorrect: false },
      { answerText: "Paris", isCorrect: true },
      { answerText: "Dublin", isCorrect: false },
    ],
  },
  {
    questionText: "Who is CEO of Tesla?",
    answerOptions: [
      { answerText: "Jeff Bezos", isCorrect: false },
      { answerText: "Elon Musk", isCorrect: true },
      { answerText: "Bill Gates", isCorrect: false },
      { answerText: "Tony Stark", isCorrect: false },
    ],
  },
  {
    questionText: "The iPhone was created by which company?",
    answerOptions: [
      { answerText: "Apple", isCorrect: true },
      { answerText: "Intel", isCorrect: false },
      { answerText: "Amazon", isCorrect: false },
      { answerText: "Microsoft", isCorrect: false },
    ],
  },
  {
    questionText: "How many Harry Potter books are there?",
    answerOptions: [
      { answerText: "1", isCorrect: false },
      { answerText: "4", isCorrect: false },
      { answerText: "6", isCorrect: false },
      { answerText: "7", isCorrect: true },
    ],
  },
];

const quiz = () => {
  const [score, setScore] = useState(0);
  const [answerNum, setAnswerNum] = useState(0);
  const [showScore, setShowScore] = useState(false);

  const handleAnswerScore = (isCorrect) => {
    const nextNum = answerNum + 1;

    if (isCorrect) {
      setScore(score + 1);
    }

    if (nextNum < questions.length) {
      setAnswerNum(nextNum);
    } else {
      setShowScore(true);
    }
  };

  return (
    <div className="app-quiz">
      {showScore ? (
        <div className="score-section">
          You scored {score} out of {questions.length}
        </div>
      ) : (
        <>
          <div className="question-section">
            <div className="question-count">
              <span>Question {answerNum + 1}</span>/{questions.length}
            </div>
            <h2 className="question-text">
              {questions[answerNum].questionText}
            </h2>
          </div>
          <ul className="answer-section">
            {questions[answerNum].answerOptions.map((answer) => {
              return (
                <li
                  className="button-quiz"
                  key={answer.answerText}
                  onClick={() => handleAnswerScore(answer.isCorrect)}
                >
                  {answer.answerText}
                </li>
              );
            })}
          </ul>
        </>
      )}
    </div>
  );
};

export default quiz;
