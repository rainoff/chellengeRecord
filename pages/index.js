import * as React from "react";

import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Copyright from "@/src/components/Copyright";

export default function Index() {
  return (
    <>
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={3}></Grid>
        <Copyright sx={{ pt: 4 }} />
      </Container>
    </>
  );
}
