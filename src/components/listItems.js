import * as React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import AssignmentIcon from "@mui/icons-material/Assignment";
import Toolbar from "@mui/material/Toolbar";
import Link from "./Link";

export const mainListItems = (
  <React.Fragment>
    <ListItemButton>
      <Link href="/chellenge/helloworld">
        <ListItemText primary="Hello world" />
      </Link>
    </ListItemButton>
    <ListItemButton>
      <Link href="/chellenge/clicker">
        <ListItemText primary="clicker" />
      </Link>
    </ListItemButton>
    <ListItemButton>
      <Link href="/chellenge/searchfilter">
        <ListItemText primary="searchfilter" />
      </Link>
    </ListItemButton>
    <ListItemButton>
      <Link href="/chellenge/registform">
        <ListItemText primary="Reports" />
      </Link>
    </ListItemButton>
    <ListItemButton>
      <ListItemText primary="Integrations" />
    </ListItemButton>
  </React.Fragment>
);

export const secondaryListItems = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      Saved reports
    </ListSubheader>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItemButton>
  </React.Fragment>
);
